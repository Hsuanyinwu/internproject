// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDha_aYHH1t5zlefDuMv-Dun7LYqp1oAUU',
    authDomain: 'internship-project-350f2.firebaseapp.com',
    databaseURL: 'https://internship-project-350f2.firebaseio.com',
    projectId: 'internship-project-350f2',
    storageBucket: 'internship-project-350f2.appspot.com',
    messagingSenderId: '954299993695'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
