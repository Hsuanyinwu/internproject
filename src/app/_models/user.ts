export interface User {
  uid: number;
  email: string;
  name?: string;
  surname?: string;
  address?: string;
  age?: number;
  race?: string;
}
