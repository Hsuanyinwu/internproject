import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AuthService } from './auth.service';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { ReactiveFormsModule } from '@angular/forms';
import { environment } from '../../environments/environment';

@NgModule({
  imports: [
    AngularFireAuthModule,
    AngularFirestoreModule.enablePersistence(),
    AngularFireModule.initializeApp(environment.firebase, 'Internship Project'),
    ReactiveFormsModule
  ],
  exports: [AngularFireAuthModule, AngularFirestoreModule, ReactiveFormsModule],
  providers: [AuthService]
})
export class CoreModule {}
