import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';

import { Observable, of } from 'rxjs';
import { switchMap, tap, take } from 'rxjs/operators';
import { User } from '../_models/user';
import { JsonPipe } from '@angular/common';

@Injectable({ providedIn: 'root' })
export class AuthService {
  public user: Observable<User>;

  // #region Ctor

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router
  ) {
    this.user = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
  }
  // #endregion

  private async oAuthLogin(provider: any) {
    try {
      provider.addScope('profile');
      provider.addScope('email');

      const credentials = await this.afAuth.auth.signInWithPopup(provider);

      const userRef: AngularFirestoreDocument<User> = await this.afs.doc(
        `Users/${credentials.user.uid}`
      );
      console.log('userRef', userRef);
      if (!credentials.additionalUserInfo.isNewUser) {
        console.log('User exists!');
        this.router.navigate(['/']);
      } else {
        await this.updateUserData(credentials.user);
        this.router.navigate(['/user-profile'], {
          queryParams: { isEditing: true, isFirstTime: true },
        });
        console.log('User doesnt exist. Creating...');
      }
    } catch (error) {
      this.handleError(error);
    }
  }

  async googleLogin() {
    try {
      const provider = new auth.GoogleAuthProvider();
      await this.oAuthLogin(provider);
    } catch (error) {
      this.handleError(error);
    }
  }

  async emailSignup(bundle) {
    try {
      const credentials = await this.afAuth.auth.createUserWithEmailAndPassword(
        bundle.email,
        bundle.password
      );

      const updateBundle = {
        name: bundle.name,
        surname: bundle.surname,
        email: bundle.email,
        uid: credentials.user.uid,
      };
      await this.updateUserData(updateBundle);
      this.router.navigate(['/user-profile'], {
        queryParams: { isEditing: true, isFirstTime: true },
      });
    } catch (error) {
      this.handleError(error);
    }
  }

  async emailLogin(bundle) {
    try {
      await this.afAuth.auth.signInWithEmailAndPassword(
        bundle.email,
        bundle.password
      );
      this.router.navigate(['/']);
      this.user.pipe(tap(v => console.log(JsonPipe.prototype.transform(v))));
    } catch (error) {
      this.handleError(error);
    }
  }

  async updateUserData(user) {
    // Sets user data to firestore on login

    try {
      const userRef = await this.afs.doc(`users/${user.uid}`);
      const data: User = {
        uid: user.uid,
        email: user.email,
        name: user.name || '',
        surname: user.surname || '',
        address: user.address || '',
        age: user.age || '',
        race: user.race || '',
      };

      await userRef.set(Object.assign({}, data));
      this.router.navigate(['/user-profile']);
    } catch (error) {
      this.handleError(error);
    }
  }

  async signOut() {
    try {
      await this.afAuth.auth.signOut();
      this.router.navigate(['/']);
    } catch (error) {
      this.handleError(error);
    }
  }

  redirect(rout: string) {
    this.router.navigate([rout]);
  }

  private handleError(error: Error) {
    alert(error);
  }
}
