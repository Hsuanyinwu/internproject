import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { AuthService } from './core/auth.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  items: Observable<any[]>;
  constructor(db: AngularFirestore, public authService: AuthService) {
    this.items = db.collection('items').valueChanges();
  }
  title = 'Internship';
}
