import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../core/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { take } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

export enum raceEnum {
  asian = 'asian',
  white = 'white',
  black = 'black',
  indian = 'indian',
}
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
})
export class UserProfileComponent implements OnInit, OnDestroy {
  // #region Properties
  profileForm: FormGroup;
  profileData = {};
  sub;
  isEditing;
  isFirstTime;
  // #endregion

  constructor(
    public authService: AuthService,
    private fb: FormBuilder,
    private route: ActivatedRoute
  ) {
    console.log('Called Constructor');

    this.route.queryParams.subscribe(params => {
      this.isEditing = params['isEditing'] || false;
      this.isFirstTime = params['isFirstTime'];
    });
  }

  async ngOnInit() {
    this.profileForm = this.fb.group({
      name: ['', [Validators.required]],
      surname: ['', [Validators.required]],
      email: ['', [Validators.required]],
      address: ['', [Validators.required]],
      age: ['', [Validators.required]],
      race: ['', [Validators.required]],
    });

    this.sub = this.authService.user.subscribe(result => {
      this.profileData = result;
      this.handleInitprofileForm();
      console.log('result', result);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  // #region profileForm Getters
  get name() {
    return this.profileForm.get('name');
  }
  get surname() {
    return this.profileForm.get('surname');
  }
  get email() {
    return this.profileForm.get('email');
  }
  get address() {
    return this.profileForm.get('address');
  }
  get age() {
    return this.profileForm.get('age');
  }
  get race() {
    return this.profileForm.get('race');
  }
  // #endregion

  // #region Service Methods

  updateUserData() {
    this.authService.updateUserData(
      Object.assign(
        {},
        this.name.value,
        this.surname.value,
        this.address.value,
        this.age.value,
        this.race.value
      )
    );
  }

  // #endregion

  // #region Local Methods

  handleOnChange(field: string) {
    this.profileData[field] = this.profileForm.get(field).value;
    console.log(this.profileData);
  }

  handleToggleEdit() {
    this.isEditing = !this.isEditing;
  }

  handleUpdateUser() {
    this.authService.updateUserData(this.profileData);
  }

  // #endregion

  // #region lifecycle methods
  handleInitprofileForm() {
    Object.keys(this.profileData).forEach(field =>
      //  console.log(this.profileForm)
      field !== 'uid'
        ? this.profileForm.get(field).setValue(this.profileData[field])
        : {}
    );
  }

  // #endregion

  // TODO: remove unused regions
}
